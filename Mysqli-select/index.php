<?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $database = "shokugeki";

    $conn = mysqli_connect($servername, $username, $password, $database);
    if (!$conn) {
        # code...
        echo "Connection Failed : " . mysqli_connect_error();
    }
    $sql = "SELECT * FROM chef";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mysqli Select</title>
</head>

<body>
    <table border = "1" cellspacing = "0" cellpadding = "5">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Bidang</th>
        </tr>
        <?php
            $result = mysqli_query($conn, $sql);
            if (mysqli_num_rows($result) > 0) {
                # code...
                while ($row = mysqli_fetch_assoc($result)) {
                    # code...
                    echo "<tr>";
                    echo "<td>" . $row['id'] . "</td>";
                    echo "<td>" . $row['name'] . "</td>";
                    echo "<td>" . $row['bidang'] . "</td>";
                    echo "</tr>";
                }
            }
        ?>
    </table>
</body>

</html>